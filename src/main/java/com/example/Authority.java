package com.example;

import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;

public class Authority implements GrantedAuthority{

    @Id
    private String authority;

    public Authority() {}

    public Authority(String authority) {
        this.authority = authority;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}

package com.example.config;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.User;
import com.example.UserRepository;

@Service
public class MongoUserDetailsService implements UserDetailsService{

	 private final UserRepository userRepository;

	    @Autowired
	    public MongoUserDetailsService(UserRepository userRepository) {
	        this.userRepository = userRepository;
	    }

	    @Override
	    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
	        User userFromDatabase = userRepository.findByUsername(login);

	        List<GrantedAuthority> grantedAuthorities = userFromDatabase.getAuthorities().stream()
	                .map(authority -> new SimpleGrantedAuthority(authority.getAuthority())).collect(Collectors.toList());

	        return new org.springframework.security.core.userdetails.User(userFromDatabase.getUsername(), userFromDatabase.getPassword(), grantedAuthorities);

	    }

}

package com.example.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.session.SessionManagementFilter;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private MongoUserDetailsService mongoUserDetailService;

	   @Bean
	    public PasswordEncoder passwordEncoder(){
	        return new BCryptPasswordEncoder();
	    }

	    @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder auth)
	            throws Exception {
	        auth.userDetailsService(mongoUserDetailService);
	        auth.authenticationProvider(authenticationProvider());
			/*
			 * .inMemoryAuthentication().withUser("ram").password("ram").roles("ADMIN"
			 * );
			 */
	    }

	    @Bean
	    public DaoAuthenticationProvider authenticationProvider(){
	        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
	        authenticationProvider.setUserDetailsService(mongoUserDetailService);
	        authenticationProvider.setPasswordEncoder(passwordEncoder());
	        return authenticationProvider;
	    }

	    @Override
	    public void configure(WebSecurity web) throws Exception {
	        web.ignoring()
	                .antMatchers(HttpMethod.OPTIONS, "/**")
	                .antMatchers("/bower_components/**", "/static/**", "/scripts/**", "/index.html","/js/**", "/html/**");

	    }

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
	                .httpBasic()
	                    .authenticationEntryPoint(new RedirectAuthenticationEntryPoint())
	                .and().rememberMe()
	                    .userDetailsService(mongoUserDetailService)
	                    .useSecureCookie(true)
	                    .tokenValiditySeconds(25000)
	                .and().authorizeRequests()
	                    .antMatchers("/index.html", "/**", "/login", "/static/**", "/html/**", "/js/**", "/scripts/**")
	                    .permitAll()
	                    .anyRequest().authenticated()
	                .and().logout()
	                    .logoutSuccessUrl("/")
	                    .invalidateHttpSession(true)
	                    .deleteCookies("JSESSIONID")
	                .and().csrf()
	                    .csrfTokenRepository(csrfTokenRepository());
	                
		}
	
	  private class RedirectAuthenticationEntryPoint implements AuthenticationEntryPoint {
		  
	        @Override
	        public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
	            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
	        }

	  }
	  
	    private Filter csrfHeaderFilter() {
	        return new OncePerRequestFilter() {
	            @Override
	            protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
	                    throws ServletException, IOException {
	                CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
	                if (csrf != null) {
	                    Cookie cookie = WebUtils.getCookie(request, "XSRF-TOKEN");
	                    String token = csrf.getToken();
	                    if (cookie == null || token != null && !token.equals(cookie.getValue())) {
	                        cookie = new Cookie("XSRF-TOKEN", token);
	                        cookie.setPath("/");
	                        response.addCookie(cookie);
	                    }
	                }
	                filterChain.doFilter(request, response);
	            }

	        };
	    }

	    /**
	     * Method csrfTokenRepository creates repository for csrf security token
	     * @return repository for csrf security token
	     */
	    private CsrfTokenRepository csrfTokenRepository() {
	        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
	        repository.setHeaderName("X-XSRF-TOKEN");
	        return repository;
	    }


}

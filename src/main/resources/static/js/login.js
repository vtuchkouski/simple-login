angular
    .module('loginApp', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
    	
	    	$locationProvider.html5Mode({
	    		  enabled: true,
	    		  requireBase: false
	    		});
//	    	$urlRouterProvider.otherwise('/');
            
        	$stateProvider
                .state('login', {
                    'url': '/login',
                    'templateUrl': 'html/login.html',
                    'controller': 'loginctrl'
                })

                .state('home', {
                    'url': '/',
                    'templateUrl': 'html/home.html',
                    'controller': 'homectrl'
                });

            $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
            $httpProvider.interceptors.push(
                    function($q, $injector, $rootScope) {
                        return {
                            responseError: function (response) {

                                if(response.status === -1) {
                                    var warning = $injector.get('warning');
                                    warning.showServerConnectionLostException();
                                    return $q.reject(response);
                                }

                                if(response.status === 403) {
                                    var $http = $injector.get('$http');
                                    var deferred = $q.defer();
                                    $http.get('/api/test').then(deferred.resolve, deferred.reject);
                                    return deferred.promise.then(function () {
                                        return $http(response.config);
                                    });
                                }

                                if(response.status === 401 && !_.isEmpty($rootScope.user)) {
                                    var warning = $injector.get('warning');
                                    warning.showServerSessionExpired();
                                    return $q.reject(response);
                                }

                                if(response.data.exception === "org.springframework.orm.ObjectOptimisticLockingFailureException") {
                                    var warning = $injector.get('warning');
                                    warning.showAlertOptimisticLockingException();
                                    return $q.reject(response);
                                }

                                return $q.reject(response);
                            }
                        };
                    }
                );

        }])
    .controller('homectrl', ['$scope', '$http',
        function ($http) {
            var vm = this;
            /*$http.get('/resource')
                .then(function (data) {
                    vm.greeting = data.data;
                })*/
        }])
    .controller('loginctrl', ['$rootScope', '$scope', '$http', '$state', 'authentication',
        function ($rootScope, $scope, $http, $state, authentication) {
    		
        	 $scope.credentials = {};

	    	 $scope.authenticated = function() {
	             return authentication.authenticated;
	         };
	         
	         $scope.login = function() {
	        	 authentication.authenticate($scope.credentials, $scope.response)
	                    .then(function(authenticated) {
	                        if (authenticated) {
	                           console.log("Success login");
	                        } else {
	                           console.log("Failed login on page");
	                        }
	                })
	          };
	          $scope.logout = authentication.logout;
        }
    ])
  
    .factory('authentication',['$rootScope', '$http', '$state', '$q',
    	function($rootScope, $http, $state, $q) {
			$rootScope.user = {};
			
			var authentication = {
					authenticated : false,
					
					authenticate: function(credentials) {
						
						var deferred = $q.defer();
						
						var headers = credentials && credentials.username ?
								{Authorization: "Basic " + btoa(credentials.username + ":" + credentials.password)} : {};
								
						$http.get('/api/user', {headers: headers, params: {"remember-me": true}})
							.success(function(data) {
								$rootScope.user = data;
								authentication.authenticated = !!data.name;
								$state.go("home");
								deferred.resolve(authentication.authenticated);
							})
							.error(function() {
								deferred.resolve(false);
								authentication.authenticated = false;
							});

						return deferred.promise;  
				          
					},
					
					logout: function() {
						$http.post("/logout", {});
						authentication.authenticated = false;
						$rootScope.user = {};
						$state.go("login");
					},
					
					init: function() {
						authentication.authenticate({}, {})
						.then(function(authenticated) {
							if(!authenticated)
								$state.go("login");
						});

					$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
	
						if(toState.name === "login" || authentication.authenticated)
							return;
	
						authentication.desireState = toState.name;
						authentication.desireParams = toParams;
	
						event.preventDefault();
					});
					}
			};
			
			return authentication;
		}
    ])
    .run(['$rootScope', '$state', 'authentication', 
        function ($rootScope, $state, authentication) {

    		authentication.init();

        }
    ]);